#
# Copyright (C) 2021 The LineageOS Project
# Copyright (C) 2023 AlphaDroid
#
# SPDX-License-Identifier: Apache-2.0
#

include $(call all-makefiles-under,$(call my-dir))
PRODUCT_SOONG_NAMESPACES += \
    vendor/gms/common

# GAPPS_BUILD_TYPE
# 0 - NO GAPPS (DEFAULT)
# 1 - CORE GAPPS
# 2 - FULL GAPPS

# If GAPPS_BUILD_TYPE=1 or GAPPS_BUILD_TYPE=2
ifneq ($(filter $(strip $(GAPPS_BUILD_TYPE)),1 2),)
# Use Google Telephony by default
TARGET_USE_GOOGLE_TELEPHONY ?= true
include vendor/gms/products/gms.mk
endif
