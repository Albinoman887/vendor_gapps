#
# Copyright (C) 2020-2021 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# GMS RRO overlay packages

# prebuilt overlays
PRODUCT_PACKAGES += \
    AOSPConfigOverlay \
    GmsConfigOverlayASI \
    PixelConfigOverlay2018 \
    PixelConfigOverlay2019 \
    PixelConfigOverlay2019Midyear \
    PixelConfigOverlay2021 \
    PixelConfigOverlayPrebuilt \
    GmsConfigOverlayTurbo \
    GoogleSettingsOverlay \
    SystemUIGXOverlay

# GAPPS_BUILD_TYPE
# 0 - NO GAPPS (DEFAULT)
# 1 - CORE GAPPS
# 2 - FULL GAPPS

# If GAPPS_BUILD_TYPE=1 or GAPPS_BUILD_TYPE=2
ifneq ($(filter $(strip $(GAPPS_BUILD_TYPE)),1 2),)

ifeq ($(strip $(TARGET_USE_GOOGLE_TELEPHONY)),true)
PRODUCT_PACKAGES += \
    GmsConfigOverlayComms \
    GmsConfigOverlayTelecom \
    GmsConfigOverlayTeleService \
    GoogleDialerOverlay \
    GmsConfigOverlayTelephony
endif

# GMS properties
PRODUCT_PRODUCT_PROPERTIES += \
    ro.opa.eligible_device=true \
    ro.boot.vendor.overlay.theme=com.android.internal.systemui.navbar.gestural;com.google.android.systemui.gxoverlay \
    ro.setupwizard.enterprise_mode=1 \
    ro.setupwizard.esim_cid_ignore=00000001 \
    ro.setupwizard.rotation_locked=true \
    setupwizard.feature.baseline_setupwizard_enabled=true \
    setupwizard.feature.show_pai_screen_in_main_flow.carrier1839=false \
    setupwizard.feature.show_pixel_tos=true \
    setupwizard.feature.skip_button_use_mobile_data.carrier1839=true \
    setupwizard.theme=glif_v3_light \
    ro.com.google.ime.theme_id=5

endif


ifeq ($(strip $(GAPPS_BUILD_TYPE)),2)

PRODUCT_PACKAGES += \
    GmsConfigOverlayCommon \
    GmsConfigOverlayContactsProvider \
    GmsConfigOverlayGeotz \
    GmsConfigOverlayGSA \
    GmsConfigOverlayPersonalSafety \
    GmsConfigOverlayPhotos \
    GmsConfigOverlaySettings \
    GmsConfigOverlaySettingsProvider \
    GmsConfigOverlaySystemUI \
    GmsConfigOverlayVAS

# Pixel RRO overlay packages
PRODUCT_PACKAGES += \
    GoogleConfigOverlay \
    PixelConfigOverlayCommon \
    PixelSetupWizardOverlay

else
PRODUCT_PACKAGES += \
   GoogleCoreConfigOverlay
endif

$(call inherit-product, vendor/gms/common/common-vendor.mk)
